#ifndef INDICATORS_H
#define INDICATORS_H

#include <QQmlApplicationEngine>

#include "fanindicator.h"
#include "pumpindicator.h"
#include "relayindicator.h"
#include "circularindicator.h"
#include "confirmslider.h"

void registerTypes()
{
    qmlRegisterType<FanIndicator>("Indicators", 1, 0, "FanIndicator");
    qmlRegisterType<PumpIndicator>("Indicators", 1, 0, "PumpIndicator");
    qmlRegisterType<RelayIndicator>("Indicators", 1, 0, "RelayIndicator");
    qmlRegisterType<CircularIndicator>("Indicators", 1, 0, "CircularIndicator");
    qmlRegisterType<ConfirmSlider>("Indicators", 1, 0, "ConfirmSlider");
}

#endif // INDICATORS_H
