#ifndef CONFIRMSLIDER_H
#define CONFIRMSLIDER_H

#include <QQuickPaintedItem>
#include <QPixmap>
#include <QRect>

#define csBACKGROUNT_IMAGE              ":/resources/sliderBackground.png"
#define csSLIDER_IMAGE                  ":/resources/slider.png"
#define csORIGINAL_WIDTH                200
#define csORIGINAL_HEIGHT               75
#define csORIGINAL_SLIDER_WIDTH         55
#define csORIGINAL_SLIDER_HEIGHT        55
#define csORIGINAL_SLIDER_DISTANCE      150
#define csORIGINAL_SLIDER_START_LEFT    10
#define csORIGINAL_SLIDER_END_RIGHT     190
#define csORIGINAL_SLIDER_END_RIGHT_MIN 180

class ConfirmSlider : public QQuickPaintedItem
{
    Q_OBJECT

public:
    ConfirmSlider(QQuickItem *parent = nullptr);
    ~ConfirmSlider();

    void paint(QPainter *painter);

public slots:
    void enable(bool enable);

signals:
    void confirmed();

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

    void resizeEvent(QResizeEvent *event);

private:
    QPixmap m_backgroungImage;
    QPixmap m_sliderImage;

    QRect m_outerRectangle;
    QRect m_sliderRectangle;

    int m_sliderStartLeft;
    int m_sliderEndRight;
    int m_sliderEndRightMin;

    int m_previousCursorPosition;

    bool m_pressed;

    double m_scaleX;
    double m_scaleY;
};

#endif // CONFIRMSLIDER_H
