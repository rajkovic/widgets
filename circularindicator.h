#ifndef CIRCULARINDICATOR_H
#define CIRCULARINDICATOR_H

#include <QQuickPaintedItem>
#include <QPen>

#define ciDEFAULT_TITLE             "Indicator"
#define ciDEFAULT_INDICATOR_COLOR   Qt::blue
#define ciDEFAULT_VALUE             0.0
#define ciDEFAULT_MAXIMUM           30.0
#define ciDEFAULT_MINIMUM           0.0
#define ciDEFAULT_PRECISION         1
#define ciTITLE_IMPLICIT_HEIGHT     15.0
#define ciCIRCLE_IMPLICIT_HEIGHT    100.0
#define ciTITLE_PIXEL_SIZE          16
#define ciNUMBER_PIXEL_SIZE         26
#define ciCIRCLE_LINE_WIDTH         12.0

#define ciIMPLICIT_WIDTH            ciCIRCLE_IMPLICIT_HEIGHT
#define ciIMPLICIT_HEIGHT           ciTITLE_IMPLICIT_HEIGHT+ciCIRCLE_IMPLICIT_HEIGHT
#define ciCIRCLE_START_ANGLE        -135*16
#define ciCIRCLE_SPAN_ANGLE         -270*16
#define ciVALUE_TO_SPAN_COEFF       -270.0/(ciDEFAULT_MAXIMUM-ciDEFAULT_MINIMUM)
#define ciVALUE_TO_STRING(x)        QString::number(x, 'f', ciDEFAULT_PRECISION)

class CircularIndicator : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(QString title READ title WRITE setTitle NOTIFY titleChanged)
    Q_PROPERTY(QColor indicatorColor READ indicatorColor WRITE setIndicatorColor NOTIFY indicatorColorChanged)
    Q_PROPERTY(qreal maximum READ maximum WRITE setMaximum NOTIFY maximumChanged)
    Q_PROPERTY(qreal minimum READ minimum WRITE setMinimum NOTIFY minimumChanged)
    Q_PROPERTY(qreal value READ value WRITE setValue NOTIFY valueChanged)

public:
    CircularIndicator(QQuickItem *parent = nullptr);
    ~CircularIndicator();

    QString title() const;
    void setTitle(const QString &title);

    QColor indicatorColor() const;
    void setIndicatorColor(const QColor &indicatorColor);

    qreal maximum() const;
    void setMaximum(const qreal &maximum);

    qreal minimum() const;
    void setMinimum(const qreal &minimum);

    qreal value() const;
    void setValue(const qreal &value);

    void paint(QPainter *painter);

signals:
    void titleChanged(QString title);
    void indicatorColorChanged(QColor indicatorColor);
    void maximumChanged(qreal maximum);
    void minimumChanged(qreal minimum);
    void valueChanged(qreal value);

private:
    void resetBackground();
    void createBackground();
    void drawBackground(QPainter *painter);
    void drawIndicator(QPainter *painter, const QRectF &rect);
    void drawNumber(QPainter *painter, const QRectF &rect);

    void calculateSpanCoefficient();
    int valueToSpan(const double &value) const;

private:
    qreal m_value;
    qreal m_maximum;
    qreal m_minimum;

    QString m_title;
    QColor m_indicatorColor;

    qreal m_spanCoefficient;
    qreal m_calculatedSpanAngle;
    QString m_valueString;

    QPixmap *m_background;
};

#endif // CIRCULARINDICATOR_H
