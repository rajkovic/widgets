#ifndef BUTTONINDICATOR_H
#define BUTTONINDICATOR_H

#include <QQuickPaintedItem>
#include <QPixmap>

#define biIMPLICIT_WIDTH    200.0
#define biIMPLICIT_HEIGHT   200.0
#define biDEFAULT_STATE     false
#define biON_COLOR          Qt::darkGreen
#define biOFF_COLOR         Qt::black

class ButtonIndicator : public QQuickPaintedItem
{
    Q_OBJECT
    Q_PROPERTY(bool state READ state WRITE setState NOTIFY stateChanged)

public:
    ButtonIndicator(const char *pressedImage, const char *releasedImage, QQuickItem *parent = nullptr);

    bool state() const;
    void setState(const bool &state);

    void paint(QPainter *painter);

protected:
    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);

signals:
    void stateChanged(bool state);
    void clicked();

private:
    void updateState(bool state);

private:
    bool m_state;
    QPixmap *m_image;
    QPixmap m_imagePressed;
    QPixmap m_imageReleased;

    bool m_enableConfirm;
};

#endif // BUTTONINDICATOR_H
