#include "confirmslider.h"

#include <QPainter>
#include <QResizeEvent>

#include <QtDebug>

ConfirmSlider::ConfirmSlider(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_backgroungImage(csORIGINAL_WIDTH, csORIGINAL_HEIGHT),
    m_sliderImage(csORIGINAL_SLIDER_WIDTH, csORIGINAL_SLIDER_HEIGHT),
    m_outerRectangle(0, 0, csORIGINAL_WIDTH, csORIGINAL_HEIGHT),
    m_sliderRectangle(10, 10, csORIGINAL_SLIDER_WIDTH, csORIGINAL_SLIDER_HEIGHT),
    m_sliderStartLeft(csORIGINAL_SLIDER_START_LEFT),
    m_sliderEndRight(csORIGINAL_SLIDER_END_RIGHT),
    m_sliderEndRightMin(csORIGINAL_SLIDER_END_RIGHT_MIN),
    m_previousCursorPosition(0),
    m_pressed(false),
    m_scaleX(1.0),
    m_scaleY(1.0)
{
    setImplicitWidth(csORIGINAL_WIDTH);
    setImplicitHeight(csORIGINAL_HEIGHT);

    m_backgroungImage.load(csBACKGROUNT_IMAGE);
    m_sliderImage.load(csSLIDER_IMAGE);

    setAcceptedMouseButtons(Qt::NoButton);
}

ConfirmSlider::~ConfirmSlider()
{

}

void ConfirmSlider::paint(QPainter *painter)
{
    painter->drawPixmap(m_outerRectangle, m_backgroungImage);
    painter->drawPixmap(m_sliderRectangle, m_sliderImage);
}

void ConfirmSlider::enable(bool enable)
{
    if(enable)
    {
        setAcceptedMouseButtons(Qt::AllButtons);
    }
    else
    {
        setAcceptedMouseButtons(Qt::NoButton);
    }
}

void ConfirmSlider::mousePressEvent(QMouseEvent *event)
{
    if(m_sliderRectangle.contains(event->pos()))
    {
        m_pressed = true;
        m_previousCursorPosition = event->pos().x();
    }

}

void ConfirmSlider::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    if(m_sliderRectangle.right() > m_sliderEndRightMin)
        emit confirmed();

    m_pressed = false;
    m_previousCursorPosition = 0;
    m_sliderRectangle.moveLeft(m_sliderStartLeft);
    update();
}

void ConfirmSlider::mouseMoveEvent(QMouseEvent *event)
{
    if(m_pressed)
    {
        m_sliderRectangle.translate(event->pos().x() - m_previousCursorPosition, 0);
        m_previousCursorPosition = event->pos().x();

        if(m_sliderRectangle.left() < m_sliderStartLeft)
            m_sliderRectangle.moveLeft(m_sliderStartLeft);
        if(m_sliderRectangle.right() > m_sliderEndRight)
            m_sliderRectangle.moveRight(m_sliderEndRight);

        update();
    }
}

void ConfirmSlider::resizeEvent(QResizeEvent *event)
{
    m_outerRectangle.setWidth(event->size().width());
    m_outerRectangle.setHeight(event->size().height());
//    m_scaleX = m_width / csORIGINAL_WIDTH;
//    m_scaleY = m_height / csORIGINAL_HEIGHT;


}
