#ifndef FANINDICATOR_H
#define FANINDICATOR_H

#include "buttonindicator.h"

#define fiPRESSED_IMAGE     ":/resources/fanPressed.png"
#define fiRELEASED_IMAGE    ":/resources/fanReleased.png"

class FanIndicator : public ButtonIndicator
{
    Q_OBJECT
public:
    FanIndicator(QQuickItem *parent = nullptr);
};

#endif // FANINDICATOR_H
