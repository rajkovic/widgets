#include "circularindicator.h"
#include <QPainter>
#include <QtDebug>

CircularIndicator::CircularIndicator(QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_value(ciDEFAULT_VALUE),
    m_maximum(ciDEFAULT_MAXIMUM),
    m_minimum(ciDEFAULT_MINIMUM),
    m_title(ciDEFAULT_TITLE),
    m_indicatorColor(ciDEFAULT_INDICATOR_COLOR),
    m_background(nullptr)
{
    setImplicitWidth(ciIMPLICIT_WIDTH);
    setImplicitHeight(ciIMPLICIT_HEIGHT);

    resetBackground();
    createBackground();

    calculateSpanCoefficient();
}

CircularIndicator::~CircularIndicator()
{
    delete m_background;
}

QString CircularIndicator::title() const
{
    return m_title;
}

void CircularIndicator::setTitle(const QString &title)
{
    m_title = title;
    resetBackground();
    createBackground();
    emit titleChanged(m_title);
}

QColor CircularIndicator::indicatorColor() const
{
    return m_indicatorColor;
}

void CircularIndicator::setIndicatorColor(const QColor &indicatorColor)
{
    m_indicatorColor = indicatorColor;
    emit indicatorColorChanged(m_indicatorColor);
}

qreal CircularIndicator::maximum() const
{
    return m_maximum;
}

void CircularIndicator::setMaximum(const qreal &maximum)
{
    m_maximum = maximum;
    calculateSpanCoefficient();
    emit maximumChanged(m_maximum);
}

qreal CircularIndicator::minimum() const
{
    return m_minimum;
}

void CircularIndicator::setMinimum(const qreal &minimum)
{
    m_minimum = minimum;
    calculateSpanCoefficient();
    emit minimumChanged(m_minimum);
}

qreal CircularIndicator::value() const
{
    return m_value;
}

void CircularIndicator::setValue(const qreal &value)
{
    m_value = value;
    update();
    emit valueChanged(m_value);
}

void CircularIndicator::paint(QPainter *painter)
{

    QRectF _circleRectangle(0.0 + ciCIRCLE_LINE_WIDTH / 2.0,
                            ciTITLE_IMPLICIT_HEIGHT + ciCIRCLE_LINE_WIDTH / 2.0,
                            ciCIRCLE_IMPLICIT_HEIGHT - ciCIRCLE_LINE_WIDTH,
                            ciCIRCLE_IMPLICIT_HEIGHT - ciCIRCLE_LINE_WIDTH);

    painter->setRenderHint(QPainter::Antialiasing);

    drawBackground(painter);
    drawIndicator(painter, _circleRectangle);
    drawNumber(painter, _circleRectangle);

}

void CircularIndicator::resetBackground()
{
    delete m_background;
    m_background = new QPixmap(static_cast<int>(implicitWidth()), static_cast<int>(implicitHeight()));
    m_background->fill(Qt::transparent);
}

void CircularIndicator::createBackground()
{
    QPainter *_painter = new QPainter(m_background);
    QFont _font = _painter->font();
    QRectF _titleRectangle(0.0,
                           0.0,
                           ciIMPLICIT_WIDTH,
                           ciTITLE_IMPLICIT_HEIGHT);

    QRectF _circleRectangle(0.0 + ciCIRCLE_LINE_WIDTH / 2.0,
                            ciTITLE_IMPLICIT_HEIGHT + ciCIRCLE_LINE_WIDTH / 2.0,
                            ciCIRCLE_IMPLICIT_HEIGHT - ciCIRCLE_LINE_WIDTH,
                            ciCIRCLE_IMPLICIT_HEIGHT - ciCIRCLE_LINE_WIDTH);
     QPen _circleBackgroundPen(Qt::gray, ciCIRCLE_LINE_WIDTH);


     _font.setPixelSize(ciTITLE_PIXEL_SIZE);

    _painter->setFont(_font);
    _painter->setRenderHint(QPainter::Antialiasing);

    _painter->setPen(Qt::white);
    _painter->drawText(_titleRectangle,
                       Qt::AlignCenter,
                       m_title);

    _painter->setPen(_circleBackgroundPen);
    _painter->drawArc(_circleRectangle,
                      ciCIRCLE_START_ANGLE,
                      ciCIRCLE_SPAN_ANGLE);

    delete _painter;
}

void CircularIndicator::drawBackground(QPainter *painter)
{
    painter->drawPixmap(0, 0, width(), height(), *m_background);
}

void CircularIndicator::drawIndicator(QPainter *painter, const QRectF &rect)
{
    QPen _circleIndicatorPen(m_indicatorColor, ciCIRCLE_LINE_WIDTH);

    painter->setPen(_circleIndicatorPen);
    painter->setBrush(m_indicatorColor);
    painter->drawArc(rect,
                     ciCIRCLE_START_ANGLE,
                     valueToSpan(m_value));
//    qDebug() << "Value: " << m_value << "; Angle: " << valueToSpan(m_value) + ciCIRCLE_START_ANGLE;
}

void CircularIndicator::drawNumber(QPainter *painter, const QRectF &rect)
{
    QFont _font = painter->font();

    _font.setPixelSize(ciNUMBER_PIXEL_SIZE);
    _font.setBold(true);
    painter->setFont(_font);

    painter->setBrush(m_indicatorColor);
    painter->drawText(rect,
                      Qt::AlignCenter,
                      ciVALUE_TO_STRING(m_value));
//    qDebug() << rect;
}

void CircularIndicator::calculateSpanCoefficient()
{
    m_spanCoefficient = -270.0 / (m_maximum - m_minimum);
}

int CircularIndicator::valueToSpan(const double &value) const
{
    return static_cast<int>((value - m_minimum) * m_spanCoefficient)*16;
}
