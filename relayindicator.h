#ifndef RELAYINDICATOR_H
#define RELAYINDICATOR_H

#include "buttonindicator.h"

#define riPRESSED_IMAGE     ":/resources/relayPressed.png"
#define riRELEASED_IMAGE    ":/resources/relayReleased.png"

class RelayIndicator : public ButtonIndicator
{
    Q_OBJECT
public:
    RelayIndicator(QQuickItem *parent = nullptr);
};

#endif // RELAYINDICATOR_H
