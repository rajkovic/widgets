#ifndef PUMPINDICATOR_H
#define PUMPINDICATOR_H

#include "buttonindicator.h"

#define piPRESSED_IMAGE     ":/resources/pumpPressed.png"
#define piRELEASED_IMAGE    ":/resources/pumpReleased.png"

class PumpIndicator : public ButtonIndicator
{
public:
    PumpIndicator(QQuickItem *parent = nullptr);
};

#endif // PUMPINDICATOR_H
