import QtQuick 2.0
import Indicators 1.0

Item {
    id: root

    FanIndicator {
        id: button

        onClicked: {
            slider.enable(true);
//            opacity = 1.0;
        }
    }

    ConfirmSlider {
        id: slider

        anchors.top: button.bottom
        anchors.left: button.left

        onConfirmed: {
            enable(false);
//            opacity = 0;
        }
    }

    Component.onCompleted: {
//        slider.opacity = 0;
    }
}
