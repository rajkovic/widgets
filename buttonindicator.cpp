#include "buttonindicator.h"

#include <QPainter>

ButtonIndicator::ButtonIndicator(const char *pressedImage, const char *releasedImage, QQuickItem *parent) :
    QQuickPaintedItem(parent),
    m_state(biDEFAULT_STATE),
    m_imagePressed(biIMPLICIT_WIDTH, biIMPLICIT_HEIGHT),
    m_imageReleased(biIMPLICIT_WIDTH, biIMPLICIT_HEIGHT),
    m_enableConfirm(false)
{
    setImplicitWidth(biIMPLICIT_WIDTH);
    setImplicitHeight(biIMPLICIT_HEIGHT);
    m_imagePressed.load(pressedImage);
    m_imageReleased.load(releasedImage);
    m_image = &m_imageReleased;
    setAcceptedMouseButtons(Qt::AllButtons);
}

bool ButtonIndicator::state() const
{
    return m_state;
}

void ButtonIndicator::setState(const bool &state)
{
    updateState(state);
    emit stateChanged(m_state);
}

void ButtonIndicator::paint(QPainter *painter)
{
    painter->drawPixmap(0, 0, biIMPLICIT_WIDTH, biIMPLICIT_HEIGHT, *m_image);
}

void ButtonIndicator::mousePressEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    m_image = &m_imagePressed;
    update();
}

void ButtonIndicator::mouseReleaseEvent(QMouseEvent *event)
{
    Q_UNUSED(event);
    m_image = &m_imageReleased;
    update();
    emit clicked();
}

void ButtonIndicator::updateState(bool state)
{
    m_state = state;
    if(m_state)
        setFillColor(biON_COLOR);
    else
        setFillColor(biOFF_COLOR);
    update();
}
